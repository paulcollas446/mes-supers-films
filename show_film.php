<?php
require_once "./src/function/Database.php";

require_once "src/models/Film.php";
require_once "src/models/Realisateur.php";

require_once "src/controller/FilmController.php";
require_once "src/controller/RealisateurController.php";

$id = $_GET['id'];

$filmController = new FilmController();
$realController = new RealisateurController();

$film = $filmController->getFilmById($id);
$realisateur = $realController->getRealById($film->getRealisateur());


include 'header.inc.php';

?>

<div class="container">
    <h2><?php echo $film->getTitle(); ?></h2>
    <img src="<?php echo $film->getJaquette();?>">
    <div class="row">
        <div class="col-8">
            <p><?php echo $film->getDesc(); ?></p>
        </div>
        <div class="col-4">
            <table class="bg-dark rounded">
                <tr>
                    <td>Réalisateur :</td>
                    <td><?php echo $realisateur->getName(); ?></td>
                </tr>
                <tr>
                    <td>Note :</td>
                    <td><?php echo $film->getNote(); ?>/5</td>
                </tr>
                <tr>
                    <td>Durée :</td>
                    <td><?php echo $film->getDuree(); ?> min</td>
                </tr>
            </table>
        </div>
    </div>
</div>




<?php

include 'footer.inc.php';
