<?php

abstract class Support
{

    protected $support_type; // magnétique, optique, numérique(cloud)

    public abstract function play();

    public abstract function stop();


    /**
     * Get the value of support_type
     */
    public function getSupport_type()
    {
        return $this->support_type;
    }

    /**
     * Set the value of support_type
     *
     * @return  self
     */
    public function setSupport_type($support_type)
    {
        $this->support_type = $support_type;

        return $this;
    }
}
