<?php

class UserController
{
    public $conn;

    public function __construct()
    {
        $db = new Database();
        $this->conn = $db->getConnection();
    }

    public function checkLogin($email, $password)
    {

        $query = "
            SELECT 
                id,    
                name,
                email,
                password
            FROM
                users
            WHERE
                email = :email
            AND
                password = :password
        ";

        $stmt = $this->conn->prepare($query);
        $stmt->execute([':email' => $email, ':password' => $password]);

        $res = array('response' => false);

        if ($stmt->rowCount() > 0) {
            $u = $stmt->fetchAll()[0];

            $user = new User();
            $user->setId($u['id']);
            $user->setName($u['name']);
            $user->setEmail($u['email']);

            $res['response'] = true;
            $res['user'] = $user;
        }

        return $res;
    }
}
